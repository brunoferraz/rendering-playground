RENDERING PLAYGROUND | INSTRUCTIONS

This is a playground to improve rendering knwoledges

# * GIT

## Cloning project
```bash
git clone [url]
```

# * Preparing needed libs

### Tucano
```bash
git clone https://gitlab.com/LCG-UFRJ/tucano.git
```

### Qt modules
```bash
sudo apt-get install qtdeclarative5-dev
```
Other Qt modules that could be interest click [here](https://docs.google.com/spreadsheets/d/16ycob0pe97H0Bz70WWXgx1-wNp6cVr1OcdQFaf5N7s8/edit#gid=0)

### Eigen
```bash
sudo apt-get install libeigen3-dev
```

### Glew and others
You will need glew and others too. Since these instalations are more complicated, i will describe it as soon as possible. Or not. rs




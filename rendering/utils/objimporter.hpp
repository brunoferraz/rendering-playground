/**
 * Tucano - A library for rapid prototying with Modern OpenGL and GLSL
 * Copyright (C) 2014
 * LCG - Laboratório de Computação Gráfica (Computer Graphics Lab) - COPPE
 * UFRJ - Federal University of Rio de Janeiro
 *
 * This file is part of Tucano Library.
 *
 * Tucano Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tucano Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tucano Library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OBJIMPORTER__
#define __OBJIMPORTER__

#include <mesh.hpp>

using namespace std;

namespace Tucano
{

namespace MeshImporter
{

#if _WIN32  //define something for Windows (32-bit and 64-bit, this part is common)
    #pragma warning(disable:4996)
#else
// avoid warnings of unused function
static void loadObjFile (Mesh* mesh, string filename) __attribute__ ((unused));
#endif

/**
 * @brief Loads a mesh from an OBJ file.
 *
 * Loads vertex coordinates and normals, texcoords and color when available.
 * @param mesh Pointer to mesh instance to load file.
 * @param filename Given filename of the OBJ file.
 */
static void loadObjFile (Mesh* mesh, string filename)
{
    vector<Eigen::Vector4f> vert;
    vector<Eigen::Vector3f> norm;
    vector<Eigen::Vector2f> texCoord;
    vector<Eigen::Vector4f> color;
    vector<Eigen::Vector3f> tang;
    vector<Eigen::Vector3f> bitang;
    vector<GLuint> elementsVertices;
    vector<GLuint> elementsNormals;
    vector<GLuint> elementsTexIDs;

    //Opening file:
    #ifdef TUCANODEBUG
    cout << "Opening Wavefront obj file " << filename.c_str() << endl << endl;
    #endif

    ifstream in(filename.c_str(),ios::in);
    if (!in)
    {
        cerr << "Cannot open " << filename.c_str() << endl; exit(1);
    }

    //Reading file:
    string line;
    while(getline(in,line))
    {

        //Vertices reading:
        if(line.substr(0,2) == "v ")
        {

            istringstream s(line.substr(2));
            Eigen::Vector4f v;
            s >> v[0]; s >> v[1]; s >> v[2]; v[3] = 1.0f;
            vert.push_back(v);

            if(s.rdbuf()->in_avail())
            {
                Eigen::Vector4f c;
                s >> c[0]; s >> c[1]; s >> c[2]; c[3] = 1.0f;
                color.push_back(c);
            }
        }

        //Normals reading:
        else if(line.substr(0,2) == "vn")
        {
            istringstream s(line.substr(3));
            Eigen::Vector3f vn;
            s >> vn[0]; s >> vn[1]; s >> vn[2];            
            norm.push_back(vn);
        }

        //Texture Coordinates reading:
        else if(line.substr(0,2) == "vt")
        {
            istringstream s(line.substr(2));
            Eigen::Vector2f vt;
            s >> vt[0]; s >> vt[1];
            texCoord.push_back(vt);
        }

        //Elements reading: Elements are given through a string: "f vertexID/TextureID/NormalID". If no texture is given, then the string will be: "vertexID//NormalID".
        else if(line.substr(0,2) == "f ")
        {
            GLuint vertexID1, normalID1, textureID1;
            istringstream s(line.substr(2));
            while(!s.eof())
            {
                s >> vertexID1;
                elementsVertices.push_back(vertexID1-1);
                if(s.peek() == '/')
                {
                    s.get();
                    if(s.peek() == '/')
                    {
                        s.get();
                        s >> normalID1;
                        elementsNormals.push_back(normalID1-1);
                    }
                    else
                    {
                        s >> textureID1;
                        elementsTexIDs.push_back(textureID1-1);
                        if(s.peek() == '/')
                        {
                            s.get();
                            s >> normalID1;
                            elementsNormals.push_back(normalID1-1);
                        }
                    }
                }
            }
        }

        //Ignoring comment lines:
        else if(line[0] == '#') { }

        //Ignoring any other lines:
        else {};
    }
    if(in.is_open())
    {
        in.close();
    }

    // load attributes found in file
    if (vert.size() > 0)
    {
        mesh->loadVertices(vert);
    }
    if (norm.size() > 0)
    {
        mesh->loadNormals(norm);
    }
    if (texCoord.size() > 0)
    {
        mesh->loadTexCoords(texCoord);
    }
    if (color.size() > 0)
    {
        mesh->loadColors(color);
    }
    if (elementsVertices.size() > 0)
    {
        mesh->loadIndices(elementsVertices);
    }
    tang.resize(elementsVertices.size());
    bitang.resize(elementsVertices.size());
    for(int i =0; i < elementsVertices.size() ; i+=3){
//    for(int i =0; i < 3 ; i+=3){
        Eigen::Vector4f v0 = vert.at(elementsVertices[i+0]);
        Eigen::Vector4f v1 = vert.at(elementsVertices[i+1]);
        Eigen::Vector4f v2 = vert.at(elementsVertices[i+2]);
        Eigen::Vector4f delta1 = v1 - v0;
        Eigen::Vector4f delta2 = v2 - v0;

        Eigen::Vector2f uv0 = texCoord.at(elementsVertices[i+0]);
        Eigen::Vector2f uv1 = texCoord.at(elementsVertices[i+1]);
        Eigen::Vector2f uv2 = texCoord.at(elementsVertices[i+2]);

        Eigen::Vector2f deltaUV1 = uv1-uv0;
        Eigen::Vector2f deltaUV2 = uv2-uv0;

        float r = 1.0 / (deltaUV1[0] * deltaUV2[1] - deltaUV1[1] * deltaUV2[0]);
        Eigen::Vector3f tangtemp    = Eigen::Vector4f((delta1 * deltaUV2[1] - delta2 * deltaUV1[1])*r).head(3);
        Eigen::Vector3f bitangtemp  = Eigen::Vector4f((delta2 * deltaUV1[0] - delta1 * deltaUV2[0])*r).head(3);
        tang[elementsVertices[i+0]] = tangtemp;
        tang[elementsVertices[i+1]] = tangtemp;
        tang[elementsVertices[i+2]] = tangtemp;
        bitang[elementsVertices[i+0]] = bitangtemp;
        bitang[elementsVertices[i+1]] = bitangtemp;
        bitang[elementsVertices[i+2]] = bitangtemp;
    }

//    for(int i = 0; i<vert.size();i++)
//    {
//        for(int j = i+1; j<vert.size();j++){
//            if((vert.at(i)==vert.at(j))) // && (norm.at(i)==norm.at(j)) && (texCoord.at(i)==texCoord.at(j)))
//            {
//                cout << i << " " <<  j << endl;
//            }
//        }
//    }

//    cout << (vert.at(0) == vert.at(1)) << endl;
    // sets the default locations for accesing attributes in shaders
    mesh->setDefaultAttribLocations();

    mesh->createAttribute("in_Tangent", tang);
    mesh->createAttribute("in_Bitangent", bitang);

//    cout << mesh->getAttribute("in_Tangent")->name << endl;

    #ifdef TUCANODEBUG
    Misc::errorCheckFunc(__FILE__, __LINE__);
    #endif
}

}
}
#endif

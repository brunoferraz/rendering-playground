#ifndef __GLWIDGET__
#define __GLWIDGET__

#include <GL/glew.h>

#include <ssao.hpp>
#include <phongshader.hpp>
#include <toon.hpp>
#include <effects/bumpmap.hpp>

#include <utils/qttrackballwidget.hpp>
#include <QImage>
#include <tucano/texture.hpp>
#include <QGLWidget>
#include <QSize>
#include <QColor>

using namespace std;

class GLWidget : public Tucano::QtTrackballWidget
{
    Q_OBJECT

public:

    explicit GLWidget(QWidget *parent);
    ~GLWidget();
    
    /**
     * @brief Initializes the shader effect
     */
    void initialize();

    /**
     * Repaints screen buffer.
     */
    virtual void paintGL();

    float ka    = 1.0;
    float kd    = 1.0;
    float ks    = 1.0;
    float shin  = 20.0;


signals:

public slots:

    /**
     * @brief Toggles mean filter flag
     */
    void toggleEffect (int id)
    {
        active_effect = id;
        updateGL();
    }
    

    /**
     * @brief Reload effect shaders.
     */
    void reloadShaders (void)
    {
        ssao->reloadShaders();
        phong->reloadShaders();
        toon->reloadShaders();
        bumpshader->reloadShaders();
        updateGL();
    }

    /**
     * @brief Modifies the SSAO global intensity value.
     * @param value New intensity value.
     */
    void setSSAOIntensity (int value)
    {
        ssao->setIntensity(value);
        updateGL();
    }

    /**
     * @brief Modifies the SSAO blur range value.
     * @param value New blur range value.
     */
    void setSSAOBlur (int value)
    {
        ssao->setBlurRange(value);
        updateGL();
    }


    /**
     * @brief Modifies the Toon quantization level.
     * @param value New quantization level.
     */
    void setToonQuantLevel (int value)
    {
        toon->setQuantizationLevel(value);
        updateGL();
    }
    void setKa (int value){
        ka = float(value)/99.0;
        bumpshader->setKa(ka);
        updateGL();
    }
    void setKd (int value){
        kd = float(value)/99.0;
        bumpshader->setKd(kd);
        updateGL();
    }
    void setKs (int value){
        ks = float(value)/99.0;
        bumpshader->setKs(ks);
        updateGL();
    }
    void setShininess (int value){
        shin = float(value);
        bumpshader->setShininess(shin);
        updateGL();
    }

    /**
     * @brief Toggle draw trackball flag.
     */
    void toggleDrawTrackball (void)
    {
        draw_trackball = !draw_trackball;
        updateGL();
    }


private:

    /// Screen-Space Ambient Occlusion Effect
    Effects::SSAO *ssao;

    /// Simple Phong Shader
    Effects::Phong *phong;

    /// Simple Phong Shader
    Effects::Toon *toon;

    Effects::BumpMap *bumpshader;
    /// ID of active effect
    int active_effect;

    /// Flag to draw or not trackball
    bool draw_trackball;

    Tucano::Texture difuse_texture;
    Tucano::Texture bump_texture;
};

#endif // GLWIDGET

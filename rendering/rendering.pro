#-------------------------------------------------
#
# Project created by QtCreator 2014-09-18T12:26:54
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = rendering
TEMPLATE = app

EIGEN_PATH  =  /usr/include/eigen3

TUCANO_PATH = $$PWD/../../tucano

BUILDDIR =  $$PWD/../build/

#GLEW_PATH = ../../../glew
#INCLUDEPATH += $$GLEW_PATH/include
#LIBS += -L$$GLEW_PATH/lib

LIBS += -lGLEW -lGLU

INCLUDEPATH +=  $$TUCANO_PATH/src $$TUCANO_PATH/effects $$EIGEN_PATH

OBJECTS_DIR =   $$BUILDDIR/obj
MOC_DIR =       $$BUILDDIR/moc
RCC_DIR =       $$BUILDDIR/rcc
UI_DIR =        $$BUILDDIR/ui
DESTDIR =       $$BUILDDIR/../bin

QMAKE_CXXFLAGS += -DTUCANODEBUG

SOURCES += main.cpp\
        mainwindow.cpp \
        glwidget.cpp

HEADERS  += mainwindow.h \        
        glwidget.hpp \
        $$PWD/effects/ssao.hpp \
        $$PWD/effects/phongshader.hpp \
        $$PWD/effects/bumpmap.hpp \
        $$PWD/effects/toon.hpp \
        $$TUCANO_PATH/src/utils/qttrackballwidget.hpp \
        $$PWD/utils/objimporter.hpp \
        $$PWD/utils/plyimporter.hpp

FORMS    += mainwindow.ui

OTHER_FILES += \
        $$PWD/effects/shaders/toonshader.frag \
        $$PWD/effects/shaders/toonshader.vert \
        $$PWD/effects/shaders/ssao.frag \
        $$PWD/effects/shaders/ssao.vert \
        $$PWD/effects/shaders/ssaofinal.frag \
        $$PWD/effects/shaders/ssaofinal.vert \
        $$PWD/effects/shaders/viewspacebuffer.frag \
        $$PWD/effects/shaders/viewspacebuffer.vert \
        $$PWD/effects/shaders/phongshader.frag \
        $$PWD/effects/shaders/phongshader.vert  \
        $$PWD/effects/shaders/bumpshader.frag \
        $$PWD/effects/shaders/bumpshader.vert \
#        $$PWD/effects/shaders/bumpshader.geom


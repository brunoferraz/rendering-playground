/**
 * Tucano - A library for rapid prototying with Modern OpenGL and GLSL
 * Copyright (C) 2014
 * LCG - Laboratório de Computação Gráfica (Computer Graphics Lab) - COPPE
 * UFRJ - Federal University of Rio de Janeiro
 *
 * This file is part of Tucano Library.
 *
 * Tucano Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tucano Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tucano Library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BUMPMAP__
#define __BUMPMAP__

#include <tucano.hpp>
#include <camera.hpp>
#include <tucano/texture.hpp>

using namespace Tucano;

namespace Effects
{

/**
 * @brief Renders a mesh using a Phong shader.
 */
class BumpMap : public Effect
{

private:

    /// BumpMap Shader
    Shader bump_shader;

	/// Default color
	Eigen::Vector4f default_color;

    Tucano::Texture map_difuse;
    Tucano::Texture map_bump;
    float ka    = 1;
    float kd    = 1;
    float ks    = 1;
    float shin  = 20;

public:

    /**
     * @brief Default constructor.
     */
    BumpMap (void)
    {
		default_color << 0.7, 0.7, 0.7, 1.0;
    }

    /**
     * @brief Default destructor
     */
    virtual ~BumpMap (void) {}

    /**
     * @brief Load and initialize shaders
     */
    virtual void initialize (void)
    {
        // searches in default shader directory (/shaders) for shader files phongShader.(vert,frag,geom,comp)
        loadShader(bump_shader, "bumpshader") ;
    }
    void tangentSpacePrecalculate(Tucano::Mesh& mesh){
        //Tucano::VertexAttribute *v = mesh.getAttribute("in_Normal");
//        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, v->location);
//        GLint size = 0;
//        glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
//        v->unbind();

//        std::cout << size << std::endl;
//        std::cout << v->getSize() << std::endl;

//        GLint* data;
//        glGetBufferParameteriv(GL_ARRAY_BUFFER,v->getLocation(),0, data);
//        std::cout << data << std::endl;
//        GLfloat info;
//        glGetFloatv(GL_VERTEX_ARRAY_BUFFER_BINDING, &info);
//        std::cout << info << std::endl;

    }
	/**
	* @brief Sets the default color, usually used for meshes without color attribute
	*/

    void setKa (float value){
        ka = value;
    }
    void setKd (float value){
        kd = value;
    }
    void setKs (float value){
        ks = value;
    }
    void setShininess (float value){
        shin = value;
    }
	void setDefaultColor ( Eigen::Vector4f& color )
	{
		default_color = color;
	}
    void setDifuseMap(Tucano::Texture& d){
        map_difuse = d;
    }
    void setBumpMap(Tucano::Texture &d){
        map_bump = d;
    }
    /** * @brief Render the mesh given a camera and light, using a Phong shader 
     * @param mesh Given mesh
     * @param camera Given camera 
     * @param lightTrackball Given light camera 
     */

    void render (Tucano::Mesh& mesh, const Tucano::Camera& camera, const Tucano::Camera& lightTrackball)
    {
        Eigen::Vector4f viewport = camera.getViewport();
        glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
//        std::cout << lightTrackball.getViewMatrix().matrix() << std::endl;
        bump_shader.bind();

        // sets all uniform variables for the phong shader
        bump_shader.setUniform("projectionMatrix",      camera.getProjectionMatrix());
        bump_shader.setUniform("modelMatrix",           mesh.getModelMatrix());
        bump_shader.setUniform("viewMatrix",            camera.getViewMatrix());
        bump_shader.setUniform("lightViewMatrix",       lightTrackball.getViewMatrix());
        bump_shader.setUniform("has_color",             mesh.hasAttribute("in_Color"));
        bump_shader.setUniform("default_color",         default_color);
        bump_shader.setUniform("map_difuse",            map_difuse.bind());
        bump_shader.setUniform("map_bump",              map_bump.bind());
        bump_shader.setUniform("ka",                    ka);
        bump_shader.setUniform("kd",                    kd);
        bump_shader.setUniform("ks",                    ks);
        bump_shader.setUniform("shin",                  shin);

        mesh.setAttributeLocation(bump_shader);

        glEnable(GL_DEPTH_TEST);
        mesh.render();
//        mesh.renderPatches();
//        mesh.renderElements();

        map_difuse.unbind();
        map_bump.unbind();
        bump_shader.unbind();
    }
};

}


#endif

#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in VS_OUT{
    vec4 color;
    vec3 normal;
    vec4 vert;
    vec2 texCoords;
    float depth;
} gs_in[];

out vec4 color;
out vec3 normal;
out vec4 vert;
out vec2 texCoords;
out float depth;
out vec3 tangent;
out vec3 bitangent;

/*out GS_OUT {
    vec4 color;
    vec3 normal;
    vec4 vert;
    vec2 texCoords;
    float depth;
} gs_out;*/

void main()
{
    vec3 deltaPos1 = vec4(gl_in[1].gl_Position - gl_in[0].gl_Position).xyz;
    vec3 deltaPos2 = vec4(gl_in[2].gl_Position - gl_in[0].gl_Position).xyz;

    vec2 deltaUV1   = gs_in[1].texCoords - gs_in[0].texCoords;
    vec2 deltaUV2   = gs_in[2].texCoords - gs_in[0].texCoords;

    float r = 1.0 / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
    vec3 tang    = (deltaPos1 * deltaUV2.y   - deltaPos2 * deltaUV1.y)*r;
    vec3 bitang  = (deltaPos2 * deltaUV1.x   - deltaPos1 * deltaUV2.x)*r;

    for(int i = 0 ; i<3 ; i++)
    {
        color     = gs_in[i].color;
        normal    = gs_in[i].normal;
        vert      = gs_in[i].vert;
        texCoords = gs_in[i].texCoords;
        depth     = gs_in[i].depth;
        tangent   = tang;
        bitangent = bitang;
        gl_Position  = gl_in[i].gl_Position;
        EmitVertex();
    }

    EndPrimitive();
}

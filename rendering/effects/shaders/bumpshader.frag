#version 330 core

in vec4 color;
in vec3 normal;
in vec4 vert;
in vec2 texCoords;
in float depth;
in vec3 tangent;
in vec3 bitangent;

//in FS_IN {
//    vec4 color;
//    vec3 normal;
//    vec4 vert;
//    vec2 texCoords;
//    float depth;
//} fs_in;

out vec4 out_Color;

uniform mat4 lightViewMatrix;
uniform mat4 viewMatrix;

uniform sampler2D map_difuse;
uniform sampler2D map_bump;

uniform float ka;
uniform float kd;
uniform float ks;
uniform float shin;

void main(void)
{

    //Acess texture maps to get difuse and bump maps
    vec3 difuseCompMap      = texture(map_difuse, texCoords).rgb;
    vec3 bumpCompMap        = texture(map_bump, texCoords).rgb;
    //normalize the disturbed normal in order to guarantee
    bumpCompMap = normalize(bumpCompMap);
    vec3 binormal = cross(normal, tangent);
    mat3 rotation = transpose(mat3(tangent, binormal, normal));

    //Transform a unity vector into light space multiplying it by the light matrix
    vec3 lightDir       = rotation * vec4(viewMatrix * inverse(lightViewMatrix) * vec4(0.0, 0.0, 1.0, 0.0)).xyz;
    lightDir            = normalize(lightDir);

    //ambientLight is used to simulate the indirect light in the enviroment
    vec3 ambientLight           = difuseCompMap * ka;

    //Difuse Light Intensity is calculated regards to the angle of light hit the surface,
    //If the bean's angle surpass 90 degress ( >PI/2 ) it is put away
    //difuseCompMap is the texture that has material color information and will have its intensity modified
    float difuseLightIntTemp    = max(dot(bumpCompMap, lightDir),0);
    vec3 difuseLight            = difuseCompMap * kd * dot(bumpCompMap, lightDir);

    //since the vertex is a point in camera space, and the camera is centered on (0, 0, 0) the camera direction can be retrived by normalize the vertex position
    //In homogeneus coordinates the diference between point and vectors is the W. This duality allows this freedon, since W have been ignored.
    vec3 viewDir        = vert.xyz;
    viewDir             = -normalize(viewDir);

    //evaluate the angle between the normal and the lighdirection.
    float ndotl         = dot(bumpCompMap, lightDir);
    //if the angle between light and the normal surpess 90 ( > PI/2) it is put away
    float specularfactor = 0;
    if(ndotl>0){
        vec3 lightReflection = normalize(reflect(-lightDir, bumpCompMap));
        specularfactor = ks * (pow(max(dot(lightReflection, normalize(viewDir)),0.0),shin));
    };
    //the vec3(1.0) is de color specular will reflect. You can change the color based on the type of material you are using
    vec3 specularLight  = vec3 (1.0)* ks * specularfactor;

    vec4 finalColor = vec4(vec3(ambientLight + difuseLight + specularLight),1.0);
    out_Color = finalColor;
//    out_Color= vec4(texCoords,0.0, 1.0);
//    out_Color = vec4(difuseCompMap,1.0);
}

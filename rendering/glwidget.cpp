#include "glwidget.hpp"
#include <QDebug>

GLWidget::GLWidget(QWidget *parent) : Tucano::QtTrackballWidget(parent)
{
    ssao = 0;
    phong = 0;
    toon = 0;
    active_effect = 0;
    draw_trackball = true;
}

GLWidget::~GLWidget()
{
    delete ssao;
    delete phong;
    delete toon;
    delete bumpshader;
}

void GLWidget::initialize (void)
{
    // initialize the widget, camera and light trackball, and opens default mesh
    Tucano::QtTrackballWidget::initialize();
//    Tucano::QtTrackballWidget::openMesh("../models/toy.obj");
//    mesh.createQuad();

    // initialize the effects
    ssao = new Effects::SSAO();
    ssao->setShadersDir("../rendering/effects/shaders/");
    ssao->initialize();

    phong = new Effects::Phong();
    phong->setShadersDir("../rendering/effects/shaders/");
    phong->initialize();

    toon = new Effects::Toon();
    toon->setShadersDir("../rendering/effects/shaders/");
    toon->initialize();

    bumpshader = new Effects::BumpMap();
    bumpshader->setShadersDir("../rendering/effects/shaders/");
    bumpshader->initialize();

    QImage image;
    QImage glimage;

    bool exemplo = false;
    if(exemplo){
//        Tucano::QtTrackballWidget::openMesh("../models/stones/stones.obj");
        image = QImage("../models/stones/stones.jpg");
        glimage = QGLWidget::convertToGLFormat(image);
        difuse_texture.create(glimage.width(), glimage.height(), glimage.bits());
//        mesh.createQuad();
        mesh.createParallelepiped(1,1,1);
        bumpshader->setDifuseMap(difuse_texture);

        image = QImage("../models/stones/stones_norm.jpg");
        glimage = QGLWidget::convertToGLFormat(image);
        bump_texture.create(glimage.width(), glimage.height(), glimage.bits());
        bumpshader->setBumpMap(bump_texture);
        bumpshader->tangentSpacePrecalculate(mesh);
    }else{
//        Tucano::QtTrackballWidget::openMesh("../models/golfball/golfball.obj");
//        Tucano::QtTrackballWidget::openMesh("../models/golfball/uvsphere.obj");
        Tucano::QtTrackballWidget::openMesh("../models/golfball/golfball_v8.obj");
//        Tucano::QtTrackballWidget::openMesh("../models/golfball/golfball_v2.ply");
//        Tucano::QtTrackballWidget::openMesh("../models/golfball/golfball_v2.ply");
//        mesh.createParallelepiped(1.0, 1.0, 1.0);
        bumpshader->tangentSpacePrecalculate(mesh);
        image = QImage(QSize(int(128),int(128)),QImage::Format_RGB16);
        image.fill(QColor(100,100,100,255));
//        image = QImage("../models/golfball/golfball.png");
        glimage = QGLWidget::convertToGLFormat(image);
        difuse_texture.create(glimage.width(), glimage.height(), glimage.bits());
        bumpshader->setDifuseMap(difuse_texture);

        image = QImage("../models/golfball/golfball.png");
//        image = QImage(QSize(int(128),int(128)),QImage::Format_RGB16);
//        image.fill(QColor(255,255,255,255));
        glimage = QGLWidget::convertToGLFormat(image);
        bump_texture.create(glimage.width(), glimage.height(), glimage.bits());
        bumpshader->setBumpMap(bump_texture);
//        light_trackball.translate(Eigen::Vector3f(0,10.0,10.0));
//        light_trackball.rotate(Eigen::Quaternionf(90, 0, 0, 1));
    }


}

void GLWidget::paintGL (void)
{
    makeCurrent();

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	if (active_effect == 0 )
	{
		phong->render(mesh, camera, light_trackball);
	}
	if (active_effect == 1)
	{
		toon->render(mesh, camera, light_trackball);
	}
	if (active_effect == 2)
	{
		ssao->render(mesh, camera, light_trackball);
	}
    if (active_effect == 3)
    {
        bumpshader->render(mesh, camera, light_trackball);
    }

    glEnable(GL_DEPTH_TEST);
    if (draw_trackball)
    {
        camera.render();
    }
}
